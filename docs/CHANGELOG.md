# Changelog

## [2.2.0](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/2.1.1...release/2.2.0) (2025-02-13)


### Features

* **app:** adapt app logo and favicon with portal theme ([eb8d537](https://gitlab.mim-libre.fr/alphabet/sondage/commit/eb8d537c1fa6d033c480422064089f446c745136))
* **cron:** add log when cron delete polls ([73b357f](https://gitlab.mim-libre.fr/alphabet/sondage/commit/73b357fa882e75f2f97d4f896a9a451dfb2ee26f))


### Bug Fixes

* **about:** change chat url redirection ([f286c24](https://gitlab.mim-libre.fr/alphabet/sondage/commit/f286c24995519a5dd9aa41e4f8e788ebb640187f))
* **libraries:** update vulnerable library ([690d71e](https://gitlab.mim-libre.fr/alphabet/sondage/commit/690d71ebee6aca1616d8a85d1c6d933b3be2030e))
* **method:** check if poll exist ([4ce4e5b](https://gitlab.mim-libre.fr/alphabet/sondage/commit/4ce4e5b1dbd7f03ee0a3e634f3b8e6b2dd2c144e))
* **node:** update npm packages to wanted version ([d4c6ef9](https://gitlab.mim-libre.fr/alphabet/sondage/commit/d4c6ef9d75b5dd477a3d8da2b163ffefc30d02e2))
* **page:** change rendez vous into creneau because ([58394ba](https://gitlab.mim-libre.fr/alphabet/sondage/commit/58394ba2dc23347d4e9936c753ea7ff3441ce260))
* **polls:** reset timezone state in creation mode ([df4316e](https://gitlab.mim-libre.fr/alphabet/sondage/commit/df4316e33f5d127aee12132c7823f0fcc4c84671))
* **redirect:** change app name and description in redirect page ([3c011e1](https://gitlab.mim-libre.fr/alphabet/sondage/commit/3c011e1cfed5c01e166527c20d52f5acf169e583))
* **redirect:** change translation for rdv and reu description ([acd3b37](https://gitlab.mim-libre.fr/alphabet/sondage/commit/acd3b37d0f430c6f9c60717fec3af63922dde1f6))
* **response:** change getter for poll type in polls page ([05174c6](https://gitlab.mim-libre.fr/alphabet/sondage/commit/05174c68dd4b7d40e6eb48b3a18adfefa3505215))
* **text:** add missing translation in validation page ([7a934e4](https://gitlab.mim-libre.fr/alphabet/sondage/commit/7a934e495ea500c1fd1d56e054676686bb82f39d))
* **text:** add missing translations in step 4 ([c0141c9](https://gitlab.mim-libre.fr/alphabet/sondage/commit/c0141c9589a44b1ee38dba520c67bcf5c3c93316))
* **text:** change app texts with new branding texts ([c515cbe](https://gitlab.mim-libre.fr/alphabet/sondage/commit/c515cbef71d9b54c2c6583c573c57b056267bb2a))
* **text:** correct some text problems ([2e8a029](https://gitlab.mim-libre.fr/alphabet/sondage/commit/2e8a029fbe6c1da15ee57752320d25406ddd3b2d))
* **text:** delete type column in management table ([7d4dbd1](https://gitlab.mim-libre.fr/alphabet/sondage/commit/7d4dbd10d6bdfd44ecfd0be3322617b519b89964))
* **text:** latest adjustments ([ac9a4fc](https://gitlab.mim-libre.fr/alphabet/sondage/commit/ac9a4fc7f058990c2cb35b3fb6eddf0307aceae0))
* **url:** add missing poll type un redirect url ([f3de423](https://gitlab.mim-libre.fr/alphabet/sondage/commit/f3de423c9432a0e33e1584c958e3940c0437c754))

### [2.1.1](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/2.1.0...release/2.1.1) (2025-01-06)


### Bug Fixes

* **date:** display expiration date value if set in bdd ([333e755](https://gitlab.mim-libre.fr/alphabet/sondage/commit/333e755ca31836b153ac4572cbc3510e8e8462e8))

## [2.1.0](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/2.0.0...release/2.1.0) (2024-12-06)


### Features

* **poll:** increase title max length ([1af3bed](https://gitlab.mim-libre.fr/alphabet/sondage/commit/1af3bedc825ea90ac034cb26af903793c7fb3e63))
* **redirect:** add button to acces to tabs in app from redirect page ([4f946f8](https://gitlab.mim-libre.fr/alphabet/sondage/commit/4f946f873ddedf54ea6bc2efc9e6cdec8cf5bf95))


### Bug Fixes

* **eslint:** remove react linter ([4484a93](https://gitlab.mim-libre.fr/alphabet/sondage/commit/4484a93c6170b80f1e7a625d4ab75ab7a7a18cc5))
* **intro:** add value if description is not set in description textfield ([745b0c4](https://gitlab.mim-libre.fr/alphabet/sondage/commit/745b0c4dc8d2ec9dfc8f4e36f82d65f06fb97ff9))
* **linter:** add new js font file to eslint ignore ([5a4a31e](https://gitlab.mim-libre.fr/alphabet/sondage/commit/5a4a31eb0f11db1b774c3cc8f36037bf14add756))
* **meeting:** sort displayed date and change modale ui ([549abcb](https://gitlab.mim-libre.fr/alphabet/sondage/commit/549abcbdffdcfc15a37b32eb3beea54b304829de))
* **package:** set package lock version to 2 ([fcafddd](https://gitlab.mim-libre.fr/alphabet/sondage/commit/fcafddd3f87767c98e4785efacf46f3e4645d915))
* **poll:** set value for description when is empty ([82137d9](https://gitlab.mim-libre.fr/alphabet/sondage/commit/82137d96171740e22c95a942df0475b429487b83))
* **redirect:** change label for accessibility button ([397f27d](https://gitlab.mim-libre.fr/alphabet/sondage/commit/397f27d3895c3122320761f78f762d5333ed44a2))
* **redirect:** change labels text and size and replace button ([fe4bd38](https://gitlab.mim-libre.fr/alphabet/sondage/commit/fe4bd383a70f08084ea1cf1c539989c9df4c4b6e))
* **sort:** improve function of sorting ([0c1a50d](https://gitlab.mim-libre.fr/alphabet/sondage/commit/0c1a50d3b4bdaf9527bc5db52ab3d9fb746afa15))

## [2.0.0](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/1.11.0...release/2.0.0) (2024-10-14)


### ⚠ BREAKING CHANGES

* **redirect:** "Sondage" becomes "Réunion" & "Rendez-vous"

### Features

* **home:** create a new redirection page ([8c93f90](https://gitlab.mim-libre.fr/alphabet/sondage/commit/8c93f90b994925566ebfe04ba583db91412d163c))
* **info:** add error display for title and poll type ([323afa4](https://gitlab.mim-libre.fr/alphabet/sondage/commit/323afa40b7ee4abc57e02608f76b5fc9c9e7404d))
* **meeting:** delete redirection after answers validation ([72e71df](https://gitlab.mim-libre.fr/alphabet/sondage/commit/72e71dfd1e4d188b950486f80ef3b4278074b16f))
* **message:** change validation message by application ([41da38d](https://gitlab.mim-libre.fr/alphabet/sondage/commit/41da38da2a3401ab3cb949c1154506c7445c9a36))
* **meteor:** update to version 2.16 ([e60fab1](https://gitlab.mim-libre.fr/alphabet/sondage/commit/e60fab1c23c5ebac7d44b608bb49aa68676ea215))
* **nav:** add home tab in nav bar ([45eca3a](https://gitlab.mim-libre.fr/alphabet/sondage/commit/45eca3ae637b301f9c222b8498ac453595255bba))
* **npm:** update npm packages ([c5fd532](https://gitlab.mim-libre.fr/alphabet/sondage/commit/c5fd5329c82220c5e5634de549e86b87666c8b0b))
* **polls:** add type management in url in admin page ([86dd280](https://gitlab.mim-libre.fr/alphabet/sondage/commit/86dd2803f1346d25c8195570c1cfa36baa30bd5b))
* **redirect:** add new visuel for demo apps ([e453a58](https://gitlab.mim-libre.fr/alphabet/sondage/commit/e453a58fe9bd325612bdbddfb5deb31db4ac90d9))
* **redirect:** add poll type in newpollstore and global store ([3b1bde3](https://gitlab.mim-libre.fr/alphabet/sondage/commit/3b1bde304d09602c2d33415cdabb3de0f66a934a))
* **redirect:** add translation for new feature redirection ([3044e5d](https://gitlab.mim-libre.fr/alphabet/sondage/commit/3044e5d0bf85cf089a13aa3498520f1ffc36d957))
* **redirect:** add translation for service text ([a6f5c35](https://gitlab.mim-libre.fr/alphabet/sondage/commit/a6f5c358e027fc8fce7ec46400f374d9069b3afd))
* **redirect:** add type management for about page ([a691cce](https://gitlab.mim-libre.fr/alphabet/sondage/commit/a691cce12dd6b452e210028db4d6fa79c622aa45))
* **redirect:** add type management in edit and cancel answer route ([06eaa21](https://gitlab.mim-libre.fr/alphabet/sondage/commit/06eaa21f29cd369de4943af28efcb2623c1f041c))
* **redirect:** add type management in edit poll route ([48aa1d8](https://gitlab.mim-libre.fr/alphabet/sondage/commit/48aa1d8cb1e08825222beda47cc5b69876b268dd))
* **redirect:** add type management in new polls creation ([1bce82e](https://gitlab.mim-libre.fr/alphabet/sondage/commit/1bce82e64787da989fcf4f063acfb95797f43cbd))
* **redirect:** change app logo and translation with context ([d73c57f](https://gitlab.mim-libre.fr/alphabet/sondage/commit/d73c57fde619738c7d779db7f60c995d2903f6ca))
* **redirect:** create new app logo for rendez-vous ([5595214](https://gitlab.mim-libre.fr/alphabet/sondage/commit/55952143ee8baeab436d1fd18634b143cf148e5c))
* **redirect:** create new route admin rm with type management ([3007cf6](https://gitlab.mim-libre.fr/alphabet/sondage/commit/3007cf6961e0472bce34b2f2ec6cbe4916bfe97c))
* **redirect:** divided meeting and poll in back methods ([388e92c](https://gitlab.mim-libre.fr/alphabet/sondage/commit/388e92ced8a6a23bf4da9047aafc7cdb4c00b2d7))
* **redirect:** first part of translation for the new feat redirect ([8216877](https://gitlab.mim-libre.fr/alphabet/sondage/commit/821687774e13575395256c91f47ec105ea46390f))
* **redirect:** hide option for questionnaire bloc with url setting ([971b0d2](https://gitlab.mim-libre.fr/alphabet/sondage/commit/971b0d24725c129ced0e1722b6a557853344e8f7))
* **redirect:** manage both instance rdv and reu in partcipate page ([d2ae486](https://gitlab.mim-libre.fr/alphabet/sondage/commit/d2ae486f55f2854ab7a1f8ccf55bf7d4cb5ed767))
* **redirect:** use router type instead of global state type ([a7662fd](https://gitlab.mim-libre.fr/alphabet/sondage/commit/a7662fd04999b785229cb659643595a21e49b812))
* **settings:** use appname setting in translation keys ([f39f96f](https://gitlab.mim-libre.fr/alphabet/sondage/commit/f39f96fb654a74f16cec828fc49653bf3aba5a7c))


### Bug Fixes

* **about:** back button go back without 404 ([0a0e685](https://gitlab.mim-libre.fr/alphabet/sondage/commit/0a0e685d8035934e05e393650a011558361e3ebd))
* **about:** back button go really back ([a228d7a](https://gitlab.mim-libre.fr/alphabet/sondage/commit/a228d7a226689f690e508330146d0510d314631e))
* **about:** fix portal name in translation string ([34cfa6b](https://gitlab.mim-libre.fr/alphabet/sondage/commit/34cfa6bfd35811db5d497490c227f3e88fa2a83d))
* **about:** set default value without meteor setting ([a3f4a4a](https://gitlab.mim-libre.fr/alphabet/sondage/commit/a3f4a4aeb2fa3bbc146e5d1d33e878ab2c249b47))
* **autocomplete:** replace simple autocomplete by svelect lib ([e00b3e8](https://gitlab.mim-libre.fr/alphabet/sondage/commit/e00b3e81efee319985b8b29cef4500f603bb6f77))
* **edition:** display timezone for meeting validation step ([b408ae7](https://gitlab.mim-libre.fr/alphabet/sondage/commit/b408ae7b2d196c0d8626a2d684ededd2f51655e4))
* **edition:** keep context when cancel polls creation ([3ac7ac6](https://gitlab.mim-libre.fr/alphabet/sondage/commit/3ac7ac670cb0d2ae059cd34a9e3cad6bf10f2735))
* **edition:** restrict some fields for poll editing ([19caa99](https://gitlab.mim-libre.fr/alphabet/sondage/commit/19caa9981463e7a995351a59478e7858205c2d8b))
* **edition:** retrieve all steps for meeting edition ([14108d9](https://gitlab.mim-libre.fr/alphabet/sondage/commit/14108d95dd7db573c1a25f5028c64bcb929731d9))
* **edition:** show local timezone by default in validation step ([bf0d9e3](https://gitlab.mim-libre.fr/alphabet/sondage/commit/bf0d9e3172131df22518ae4e79d98e8cb84433fa))
* **info:** clarify selection between public and group poll ([fb48708](https://gitlab.mim-libre.fr/alphabet/sondage/commit/fb487082062dbd1976af923c62f0f068d899e2f7))
* **info:** highlight disabled button ([bc9c940](https://gitlab.mim-libre.fr/alphabet/sondage/commit/bc9c940ed3f5dcbab94bbaad1c9bfd9ad5507c24))
* **infostep:** change group selector ([7edd582](https://gitlab.mim-libre.fr/alphabet/sondage/commit/7edd58223590a37142223f566cdcf084272dea5b))
* **infostep:** set new poll type when come from portal  apps ([fc1addb](https://gitlab.mim-libre.fr/alphabet/sondage/commit/fc1addbeafeab1cc73c44cf9fd11779deba4ec02))
* **lib:** fix svelte toast import in many files ([3f23870](https://gitlab.mim-libre.fr/alphabet/sondage/commit/3f23870dfb4c193b185f37654fa3572e28703838))
* **node:** update package lock version to 2 ([d1dcb56](https://gitlab.mim-libre.fr/alphabet/sondage/commit/d1dcb5642e5b16f7b1e2ed8a4c9dfe199a17abbf))
* **npm:** fix conflict on package-lock.json ([732ee5a](https://gitlab.mim-libre.fr/alphabet/sondage/commit/732ee5a0d89743b1f46cb711560359b4aad83159))
* **packages:** fix bad package-lock.json file ([aec8feb](https://gitlab.mim-libre.fr/alphabet/sondage/commit/aec8feba964770a3464aef73cf8c71bb17146825))
* **redirect:** keep redirection in same browser tab ([19b0fab](https://gitlab.mim-libre.fr/alphabet/sondage/commit/19b0fab1a280770697332556baa7729e6084d53e))
* **redirect:** rework edit route params and calls ([2fa49f5](https://gitlab.mim-libre.fr/alphabet/sondage/commit/2fa49f5e8ca1aff7d299e7053385468e7336f90d))
* **settings:** replace questionnaireUrl with questionnaireURL ([a547b4b](https://gitlab.mim-libre.fr/alphabet/sondage/commit/a547b4b1d67063a8879a1a803aaab98fabe5aaec))
* **translation:** change some labels ([651b297](https://gitlab.mim-libre.fr/alphabet/sondage/commit/651b29728d8340d48b79d0c7159490aaaa929d7e))
* **translation:** in french, colon is preceded by a dot ([c961b33](https://gitlab.mim-libre.fr/alphabet/sondage/commit/c961b33c0d54c05be23ae09d41b0140fcbc11514))
* **translation:** replace backticks by single quotation marks ([80920b9](https://gitlab.mim-libre.fr/alphabet/sondage/commit/80920b9b9ca4de4be327b5e5a4ef6101c4206e8e))


### Documentation

* **settings:** add doc for new setting questionnaire url ([b3de551](https://gitlab.mim-libre.fr/alphabet/sondage/commit/b3de5514448f265ab38c1b941077b8ddef102aaa))


### Code Refactoring

* **redirect:** rework back to portal button ([0727af8](https://gitlab.mim-libre.fr/alphabet/sondage/commit/0727af8bb67142cd48d440908dcdfe2e683f665e))

## [1.11.0](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/1.10.0...release/1.11.0) (2024-06-17)


### Features

* **about:** add link in footer and some text ([526f034](https://gitlab.mim-libre.fr/alphabet/sondage/commit/526f03473ee9054deee27353862bf83a16fe4825))
* **about:** build first version of about page ([091d03b](https://gitlab.mim-libre.fr/alphabet/sondage/commit/091d03bddc38632f8814ace8dd1dd983125f5a93))
* **about:** last modification in about text ([bc8bc14](https://gitlab.mim-libre.fr/alphabet/sondage/commit/bc8bc14993c612acd4374071cd50e76ea4f7f6b7))
* **answer:** change layout for connection infos ([0e627cc](https://gitlab.mim-libre.fr/alphabet/sondage/commit/0e627cc3f31eff4020b7bf97a2bd8e8d9708b6f5))
* **answer:** change text fro infos completion ([5517b1d](https://gitlab.mim-libre.fr/alphabet/sondage/commit/5517b1d9384d39efa427a93b89ea776d594e2482))
* **mail:** add ical event in owner mail when user validate a slot ([a9480ec](https://gitlab.mim-libre.fr/alphabet/sondage/commit/a9480ecbff6226f6e10f5498be2a5bcf6fb7aad3))
* **meetings:** change ical description in email to poll owner ([9d5fb4a](https://gitlab.mim-libre.fr/alphabet/sondage/commit/9d5fb4a756419a79bb3324a871f0856ba02d04ae))
* **meteor:** update meteor and packages to version 2.15 ([569cb87](https://gitlab.mim-libre.fr/alphabet/sondage/commit/569cb873fddb0a160d5e78e207b272ed6d8ea31a))
* **meteor:** update meteor packages ([be1e289](https://gitlab.mim-libre.fr/alphabet/sondage/commit/be1e2892936cc3cd4f579a0ac5144020cebffe39))
* **meteor:** update meteor version ([d0b66c5](https://gitlab.mim-libre.fr/alphabet/sondage/commit/d0b66c586cd273d40ff6d2550fd6eb234d63fe72))
* **node:** update npm outdated packages ([d6f8393](https://gitlab.mim-libre.fr/alphabet/sondage/commit/d6f83930b57a0461428a1f6682f771d797b3b5c8))
* **npm:** update npm packages ([794f8a1](https://gitlab.mim-libre.fr/alphabet/sondage/commit/794f8a125ef9a31af1890791176161d37dc00109))
* **poll:** add management for public or group poll ([5b55444](https://gitlab.mim-libre.fr/alphabet/sondage/commit/5b554444e8725fab4ebadd41cf93fca072230d8b))
* **polls:** add poll expiration feature ([26845df](https://gitlab.mim-libre.fr/alphabet/sondage/commit/26845df38febb0a5055fd1dfc01111cc041ff774))
* **polls:** completed polls are still accessible to participants ([b98a856](https://gitlab.mim-libre.fr/alphabet/sondage/commit/b98a8561c98a962033494a801aec5fce8952347c))
* **timezone:** time zone management in polls ([36ed069](https://gitlab.mim-libre.fr/alphabet/sondage/commit/36ed0697c9b28260afd5c1ba155658cb83ebe7e4))


### Bug Fixes

* **about:** correct grammatical errors ([133c379](https://gitlab.mim-libre.fr/alphabet/sondage/commit/133c3798eef6eb59043d885908ffbcf0fcedf703))
* **answer:** fix connected answer ([cadce08](https://gitlab.mim-libre.fr/alphabet/sondage/commit/cadce08baa356436bbb31e8f254968b917c3ed71))
* **events:** always create an event when validating poll date ([acc806c](https://gitlab.mim-libre.fr/alphabet/sondage/commit/acc806c37bce3584e1e5448bc960299b8f00c8e0))
* **libraries:** update vulnerable libraries and adapt application ([caed9bb](https://gitlab.mim-libre.fr/alphabet/sondage/commit/caed9bb7b7cdf08bf3877e6b3e0f84886a3d6607))
* **meetings:** always send email to creator when answering to poll ([0b3e441](https://gitlab.mim-libre.fr/alphabet/sondage/commit/0b3e441a88e48a0141a97285d5672f88a363da3b))
* **poll:** fix link in group notification ([2b5356f](https://gitlab.mim-libre.fr/alphabet/sondage/commit/2b5356f5249d07c11c3d78eec557b70f43b6fce9))
* **polls:** conditionnal fields in polls creation validation step ([d92a78f](https://gitlab.mim-libre.fr/alphabet/sondage/commit/d92a78ff20dd4b89da29d7a7b1bb501f3068a3d6))
* **polls:** conditionnal imputs in polls creation first step ([fb0d66a](https://gitlab.mim-libre.fr/alphabet/sondage/commit/fb0d66ad9c555bf4f11f9769c468152af935bd1b))
* **polls:** purge associated pollanswers when deleting an expired poll ([1a7a8b3](https://gitlab.mim-libre.fr/alphabet/sondage/commit/1a7a8b30cd2bf63d62952374538dc124cd0b9c0e))
* **polls:** send email to all participants when validating final date ([663f874](https://gitlab.mim-libre.fr/alphabet/sondage/commit/663f874ed1579cd8b9e1d5462b0e66bbd31256aa))
* **tests:** add timeout on before each ([78e1484](https://gitlab.mim-libre.fr/alphabet/sondage/commit/78e148405e663e37e34b3c4f4d0d64875a366e97))
* **validation:** better poll validation form ([d67002d](https://gitlab.mim-libre.fr/alphabet/sondage/commit/d67002df997e001915dd1b5285bf3698138a7ab7))
* **validation:** make displying of groups conditional ([efea183](https://gitlab.mim-libre.fr/alphabet/sondage/commit/efea18386983523ccd0d0d13b2020aacce0e9465))

## [1.10.0](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/1.9.0...release/1.10.0) (2024-01-30)


### Features

* **answer:** show name and email fields as required when answering ([d53731c](https://gitlab.mim-libre.fr/alphabet/sondage/commit/d53731cab0ea4051fec0160d59c11c72370bd75e))
* **events:** add eventtype to schema ([a9d8254](https://gitlab.mim-libre.fr/alphabet/sondage/commit/a9d825499e20b096f848852318df6f92e0f3efaf))
* **faker:** change outdated faker librairie to new faker lib ([4077774](https://gitlab.mim-libre.fr/alphabet/sondage/commit/40777743c23026b004c51374cee6b6d8b1dc3bf9))
* **lib:** update outdated librairies to wanted version ([6480e22](https://gitlab.mim-libre.fr/alphabet/sondage/commit/6480e2273d39e7e43312d7f177de2a5d63e55d19))
* **meeting:** adapt ui in edit mode ([a8c5820](https://gitlab.mim-libre.fr/alphabet/sondage/commit/a8c5820195610655f522f680ce2fee40141c24a3))
* **meeting:** authorize owner to add slots ([da4ddda](https://gitlab.mim-libre.fr/alphabet/sondage/commit/da4ddda6896dcee2a8b011468d6577414fc2ccd1))
* **meetings:** adapt poll edition to new meetingSlot structure ([0d62895](https://gitlab.mim-libre.fr/alphabet/sondage/commit/0d628958965dabd334eb216221e81fbeb98b974e))
* **meetings:** add contact information in event title/description ([2ee9846](https://gitlab.mim-libre.fr/alphabet/sondage/commit/2ee98463ad5456b5c956e113d7faf763b6faf062))
* **meetings:** allow user to choose several meeting slots ([2437764](https://gitlab.mim-libre.fr/alphabet/sondage/commit/2437764928206809d45fb0a12a14b1cc31ff584a))
* **meetings:** display meeting slots per year in list mode ([95c31ef](https://gitlab.mim-libre.fr/alphabet/sondage/commit/95c31efe9420d776533df963c2ef8feb6d5770b2))
* **meetings:** remove agenda events and email creator when cancelling ([028246c](https://gitlab.mim-libre.fr/alphabet/sondage/commit/028246cd66b194822f02defc388fd90381c9c33e))
* **meteor:** update meteor and packages to 2.13.3 ([31735f6](https://gitlab.mim-libre.fr/alphabet/sondage/commit/31735f6f51cd6c5467688f883e839fe64d148d50))
* **npm:** update outdated libs to wanted version ([010f42d](https://gitlab.mim-libre.fr/alphabet/sondage/commit/010f42d079e89a008a8ea8348b55ed8f3b152c46))
* **poll:** redirect public user when he respond to a poll ([e0ecc39](https://gitlab.mim-libre.fr/alphabet/sondage/commit/e0ecc39b8a1b667e1c6d5492e24f3258e276de22))
* **polls:** add checkbox for hide participants ([ce900db](https://gitlab.mim-libre.fr/alphabet/sondage/commit/ce900db5a69180d09f5f555495a2804e8d48832f))
* **sass:** change deprecated node sass lib to sass lib ([0572fe1](https://gitlab.mim-libre.fr/alphabet/sondage/commit/0572fe11a680d3a55e1cc5c7babd57f3b822adaa))
* **toast:** update toast librairie ([009e82d](https://gitlab.mim-libre.fr/alphabet/sondage/commit/009e82d6bd47803cb1da21d928cd39e05ac912d1))


### Bug Fixes

* **answers:** fix answer count in table ([dcf7064](https://gitlab.mim-libre.fr/alphabet/sondage/commit/dcf7064e0b305fe93ab476dfac48f1b7a48a6b43))
* **answer:** update translation for required fields ([ed473f9](https://gitlab.mim-libre.fr/alphabet/sondage/commit/ed473f913345747dbd611e46f5c6194902e0720d))
* **dockerfile:** update process to generate docker image ([a6401ab](https://gitlab.mim-libre.fr/alphabet/sondage/commit/a6401ab97e4974ae293ae554fe3938ceba571d9a))
* **emails:** error sending email does not prevent answer validation ([bcf60bf](https://gitlab.mim-libre.fr/alphabet/sondage/commit/bcf60bf7b1c4b9b0b16665496b9df6992dc21b27))
* **meeting:** remove not-allowed cursor for confirmed slots ([5508341](https://gitlab.mim-libre.fr/alphabet/sondage/commit/5508341cf3bba2dd38e120cc8b3786237ece131a))
* **meetings:** fix fetching meetingSlots for old format pollAnswers ([5a95244](https://gitlab.mim-libre.fr/alphabet/sondage/commit/5a95244c4b9afa92bf261307292f1c50e5078f58))
* **meeting:** show spinner on buttons during long operations ([7a52b49](https://gitlab.mim-libre.fr/alphabet/sondage/commit/7a52b49fa11360d8f4cf47802d50f6dee05512e9))
* **meetings:** take old format into account in MeetingAnswersList.svelte ([2cd3264](https://gitlab.mim-libre.fr/alphabet/sondage/commit/2cd326498a03358dfac276586486dceeeaaf5b6c))
* **poll:** fix app crash when create poll ([71d77c2](https://gitlab.mim-libre.fr/alphabet/sondage/commit/71d77c209993238b3d9d86475d694cf4be04d294))
* **polls:** fix publication and add test ([a27266e](https://gitlab.mim-libre.fr/alphabet/sondage/commit/a27266e3d55048f3a352ede5d9c03152ab5b0b87))
* **polls:** poll owner can always see participants list ([abfd1f0](https://gitlab.mim-libre.fr/alphabet/sondage/commit/abfd1f0be88dd944014d62f7f0520fa870dac064))
* **style:** add missing css class for slot unavailable ([5e3616f](https://gitlab.mim-libre.fr/alphabet/sondage/commit/5e3616f57a0b31e51f3805aedd1924418373c020))

## [1.9.0](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/1.8.1...release/1.9.0) (2023-11-07)


### Features

* **calendar:** change button order and set default view ([2b38f41](https://gitlab.mim-libre.fr/alphabet/sondage/commit/2b38f417415b6425cf829fc6fd9c964c2f5f2c2e))
* **calendar:** change default view in meeting answer ([b3559d1](https://gitlab.mim-libre.fr/alphabet/sondage/commit/b3559d134e768d5287bdcd4a14b3e303377314be))
* **date:** add scrolling for date picking in meetings and polls step 2 ([c5ce164](https://gitlab.mim-libre.fr/alphabet/sondage/commit/c5ce164736aa0874931572e83de2ed11296286e2))
* **libs:** update docker node image ([4139076](https://gitlab.mim-libre.fr/alphabet/sondage/commit/4139076d8d3d79e0c44418284b409f4cb058fefb))
* **libs:** updated npm libs to wanted version ([ef25d32](https://gitlab.mim-libre.fr/alphabet/sondage/commit/ef25d32d3c0989c58b4a547548d2ea73da9ddcd8))
* **matomo:** integrate matomo client in App.svelte ([0c6dbc9](https://gitlab.mim-libre.fr/alphabet/sondage/commit/0c6dbc90d315d2f63144307d4277be9940488129))
* **meeting:** add more slots choices ([2a91c68](https://gitlab.mim-libre.fr/alphabet/sondage/commit/2a91c68dc74ec367350c41b1a6f1390a14c5f22d))
* **node:** change node version to previous version ([70c8a4b](https://gitlab.mim-libre.fr/alphabet/sondage/commit/70c8a4b6016f353390c97e0c2d1eba8cf0841dfc))
* **poll:** display author and creation date on poll answer page ([5e35cee](https://gitlab.mim-libre.fr/alphabet/sondage/commit/5e35ceea9a4ff13269b9b461dac71ccde44c7503))
* **style:** add style to redirection page ([2376379](https://gitlab.mim-libre.fr/alphabet/sondage/commit/2376379bfcc352c18c97257b6f2941e428fb5aa8))


### Bug Fixes

* **emails:** don't send email to poll creator (only for meetings) ([1aee8c9](https://gitlab.mim-libre.fr/alphabet/sondage/commit/1aee8c92bce6cc907ab3423f5ff543efe8a85518))
* **fr.json:** spelling correction ([7eb8abb](https://gitlab.mim-libre.fr/alphabet/sondage/commit/7eb8abbfb8f666a4d3d2bb4b03157ad9368f54cb))
* **fr.json:** spelling corrections ([b8c3e4b](https://gitlab.mim-libre.fr/alphabet/sondage/commit/b8c3e4bf0165bc0bed2ddd3b8cec17a1d88cbee5))
* **i18n:** update messages when answer on public meeting ([a3f08a6](https://gitlab.mim-libre.fr/alphabet/sondage/commit/a3f08a68305ff1a49e07e636613ff0104412a84f))
* **meetings:** notification when a meeting has been edited/cancelled ([52d273c](https://gitlab.mim-libre.fr/alphabet/sondage/commit/52d273c08d6eda125a82f5be7e6d3ea6454421f2))
* **poll:** display last modification date instead of creation date ([3d5b197](https://gitlab.mim-libre.fr/alphabet/sondage/commit/3d5b197c9a64977e81eb15af2faa0cda84ef5465))
* **polls:** disable button to not be able to create private polls ([929ef97](https://gitlab.mim-libre.fr/alphabet/sondage/commit/929ef97d0f576ea1981c3984de33f6a60773774d))
* **smtp:** better error message when sending email fails ([ec23333](https://gitlab.mim-libre.fr/alphabet/sondage/commit/ec2333340e7d7454611bef88ada70b720294e05f))
* **timeslots:** sort days at TimeStep and ValidationStep ([135496d](https://gitlab.mim-libre.fr/alphabet/sondage/commit/135496d99006173c3bce2e31249aa213450b85b8))
* **timeslots:** sort poll time slots at validation step ([8950fed](https://gitlab.mim-libre.fr/alphabet/sondage/commit/8950fed1159c063666b1002036d46ab1b695235a))

### [1.8.1](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/1.8.0...release/1.8.1) (2023-08-28)


### Bug Fixes

* **answer:** don't show metting modal in poll mode ([4e25cba](https://gitlab.mim-libre.fr/alphabet/sondage/commit/4e25cba1c8099264167a0c7f3bf52250031d644e))
* **publication:** getCurrentUser sends no answers if not connected ([edd4e06](https://gitlab.mim-libre.fr/alphabet/sondage/commit/edd4e0615ba6719ca3668e6cfca4e4feb594cf53))

## [1.8.0](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/1.7.0...release/1.8.0) (2023-08-24)


### Features

* **calendar:** fix calendar height ([491c6af](https://gitlab.mim-libre.fr/alphabet/sondage/commit/491c6afce19d50fcf483af5c16b2706f7095dd13))
* **meeting:** add 5min slot for meetings ([bb804a3](https://gitlab.mim-libre.fr/alphabet/sondage/commit/bb804a3be1ab16ed72764094c550eb5a918548de))
* **meteor:** change meteor base version ([42174a2](https://gitlab.mim-libre.fr/alphabet/sondage/commit/42174a2433dd6d867ab8f4ed74666b4e4cd230c3))
* **structure:** block creation if user is not admin ([df3f456](https://gitlab.mim-libre.fr/alphabet/sondage/commit/df3f45653a1b8d0ccccfb8fd173cd245464bcd96))


### Bug Fixes

* **answer:** can now modify a meeting answer ([60e9a02](https://gitlab.mim-libre.fr/alphabet/sondage/commit/60e9a021b91f892b0ce063c31ae2dbf95cdabf16))
* **answer:** reword new page text ([6dc2a0e](https://gitlab.mim-libre.fr/alphabet/sondage/commit/6dc2a0e66417e7cb83548c82cd6df4cf1ee73b94))
* **emails:** add timezone information to dates in emails ([81e8c50](https://gitlab.mim-libre.fr/alphabet/sondage/commit/81e8c50d4aab0b7d0d0c37d774efafbc164bc49c))
* **notification:** modify call for multigroups notification ([08529d2](https://gitlab.mim-libre.fr/alphabet/sondage/commit/08529d20f9766b41aa5eea6cbe7debb44b3ab7a7))
* **settings:** remove enableKeycloak setting (Keycloak is mandatory) ([bedf197](https://gitlab.mim-libre.fr/alphabet/sondage/commit/bedf197c588aebc306aa98e71311dfe83e2a77af))
* **settings:** update config sample and readme ([2136883](https://gitlab.mim-libre.fr/alphabet/sondage/commit/2136883ea11916aab45df2d02190818ac8618f6b))
* **store:** update store on blur only ([a84a90f](https://gitlab.mim-libre.fr/alphabet/sondage/commit/a84a90f4ca13d1c700a9ae4965ffe7e67cdb254a))


### Build System

* **meteor:** update meteor to 2.12 ans meteor libs ([23a5bea](https://gitlab.mim-libre.fr/alphabet/sondage/commit/23a5bea5b114cdca229b9abde4d42cc908ed4c20))

## [1.7.0](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/1.6.0...release/1.7.0) (2023-04-18)


### Features

* **input validation:** validate user string inputs ([a3206c7](https://gitlab.mim-libre.fr/alphabet/sondage/commit/a3206c78fe4ed3956979342ebc6ce0bc55eb5865))


### Bug Fixes

* **answer:** add tooltip and center lock icon ([fb42ff8](https://gitlab.mim-libre.fr/alphabet/sondage/commit/fb42ff8cb18ced56f459fc84a1d4951ca336d80a))
* **events:** add createdAt/updatedAt to events schema ([2964a88](https://gitlab.mim-libre.fr/alphabet/sondage/commit/2964a88042371c8d39d2bda9271f851ca9479a72))
* **events:** change some meteor methods to internal backend functions ([365719a](https://gitlab.mim-libre.fr/alphabet/sondage/commit/365719aaafaccf26e35b09643672f0c64be3f9f2))
* **fullcalendar:** downgrade lib fullcalendar ([f3dc861](https://gitlab.mim-libre.fr/alphabet/sondage/commit/f3dc8612e9379af1b247a409845b9be4cea011e6))
* **fullcalendar:** downgrade lib fullcalendar ([db611d8](https://gitlab.mim-libre.fr/alphabet/sondage/commit/db611d8b4bdb04fa27cd408d76db80780f311544))
* **tooltip:** change tooltip declaration ([fb7fa44](https://gitlab.mim-libre.fr/alphabet/sondage/commit/fb7fa44d98a9050524b9d5ccec9491ae8bb5ed28))

## [1.6.0](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/1.5.0...release/1.6.0) (2023-01-30)


### Features

* **groups:** display structure groups correctly ([4cdce24](https://gitlab.mim-libre.fr/alphabet/sondage/commit/4cdce244b93153121bd69cd0c19611c2070e6e3f))
* **meetings:** add meeting answer cancel and edit page ([62ba40b](https://gitlab.mim-libre.fr/alphabet/sondage/commit/62ba40b5926b72470d3ceb4a20c69744c5561c2c))
* **meetings:** allow poll owner to edit meeting slot for an answer ([cab6c99](https://gitlab.mim-libre.fr/alphabet/sondage/commit/cab6c99af32ccb1b62b61e6cc2d1b13e092bf84a))
* **meetings:** check user input on meeting answer edit ([20281e8](https://gitlab.mim-libre.fr/alphabet/sondage/commit/20281e828bccb41398e3baa5de5c52ee7020c489))
* **meteor:** update meteor to 2.8.1 ([f1b5cca](https://gitlab.mim-libre.fr/alphabet/sondage/commit/f1b5ccae41c392cfbfe39036ea25e4d57bd86d04))
* **packages:** update meteor to 2.8.0 and others packages ([3771692](https://gitlab.mim-libre.fr/alphabet/sondage/commit/3771692c4576357b1f6686c262e457c39dea99f3))
* **poll creation:** don't allow duplicate time slots ([dd841b1](https://gitlab.mim-libre.fr/alphabet/sondage/commit/dd841b143a48158edf3d98a871397c1556d02ff3))
* **poll:** add divider in poll answer ([df5de28](https://gitlab.mim-libre.fr/alphabet/sondage/commit/df5de28b808973390a06f4caeac8624e0680b21e))
* **polls:** add ten minutes step ([d6e1e50](https://gitlab.mim-libre.fr/alphabet/sondage/commit/d6e1e5017f0582fae0ca7bc957de4b35a3495597))


### Bug Fixes

* **edition:** don't add erroneous values at group selection ([d445769](https://gitlab.mim-libre.fr/alphabet/sondage/commit/d445769dbb96321c778f048fd8fac9b1755730c5))
* **navigation:** add condition on previous button in step 4 ([08907f0](https://gitlab.mim-libre.fr/alphabet/sondage/commit/08907f0f2926e4551e4dd6f7c6ab9dceeaeb7c0e))


### Tests

* **meeting:** add tests for poll_answers edit, cancel, get ([16b95c5](https://gitlab.mim-libre.fr/alphabet/sondage/commit/16b95c5f8f709a040b40fb9b55978e258f20ad3c))

## [1.5.0](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/1.4.0...release/1.5.0) (2022-11-22)


### Features

* **meeting:** add validation modal when answer to a meeting ([c7f9b00](https://gitlab.mim-libre.fr/alphabet/sondage/commit/c7f9b000663fa5f043fc727cb172b558b9589d8c))
* **meeting:** send email when user submit date ([ce0914d](https://gitlab.mim-libre.fr/alphabet/sondage/commit/ce0914d168027513ecc74e6ca6284bedba0709dd))
* **modal:** add edit button in validation modal ([f54e5fa](https://gitlab.mim-libre.fr/alphabet/sondage/commit/f54e5fa059e83775abe21bd8ebc5e392e23ef781))


### Bug Fixes

* **footer:** use settings subscription in store for footer ([95960bd](https://gitlab.mim-libre.fr/alphabet/sondage/commit/95960bdd4cbb40636759cfddb08b74e1457a288d))
* **label:** change label in meeting creation ([7a7c219](https://gitlab.mim-libre.fr/alphabet/sondage/commit/7a7c21921bb01719e3f0787276938f46597364ce))
* **methods:** delete meteor istest for send email method ([5984c11](https://gitlab.mim-libre.fr/alphabet/sondage/commit/5984c11019cb4015201fcb934aa84145512a67d3))
* **pollanswer:** disable redirect for no connected user ([5d2a2d9](https://gitlab.mim-libre.fr/alphabet/sondage/commit/5d2a2d9f7ba5e136f56529ed064793a907f50bd9))
* **pollAnswer:** store userId in poll answer ([07ad771](https://gitlab.mim-libre.fr/alphabet/sondage/commit/07ad7718fc948ca113aaa2d310e901281aacd6ba))
* **poll:** disabled redirect for no connected user ([8f4c436](https://gitlab.mim-libre.fr/alphabet/sondage/commit/8f4c436d39dc87537a242287df8f4f8b49a65037))
* **test:** add optional into fields and delete label ([cd9ccdc](https://gitlab.mim-libre.fr/alphabet/sondage/commit/cd9ccdcde4ec0b12269d6dac1fc357f83e713f09))
* **ui:** fix divider in selected date in meeting creation ([6f18ed0](https://gitlab.mim-libre.fr/alphabet/sondage/commit/6f18ed00a03307d80f6f72eb5a551596bdce9d55))
* **users:** fix published user fields ([657d1f3](https://gitlab.mim-libre.fr/alphabet/sondage/commit/657d1f330b8ee22c3b4522ec7006c89a131ef9a6))
* **users:** publish keycloak.name to client ([469c0bb](https://gitlab.mim-libre.fr/alphabet/sondage/commit/469c0bbcee8ecd34fbf5bb632b1b7a2559007374))


### Tests

* **app:** add all missing tests ([c25ff2d](https://gitlab.mim-libre.fr/alphabet/sondage/commit/c25ff2da08a5fcc30a6c505bb38d886507941dc9))
* **app:** add two missing tests ([7bd630c](https://gitlab.mim-libre.fr/alphabet/sondage/commit/7bd630c8688eebc9dc7ae72689a1594bbd54319f))

## [1.4.0](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/1.3.0...release/1.4.0) (2022-09-19)


### Features

* **autologin:** allow to log in and proceed on poll creation ([28c70f0](https://gitlab.mim-libre.fr/alphabet/sondage/commit/28c70f04b396a9792090ef46471971360cf14073))
* **event:** add organize of event in mail template ([8ad8f8a](https://gitlab.mim-libre.fr/alphabet/sondage/commit/8ad8f8a434cf3825d5ac2caae7eaee967718f9f5))
* **mail:** add firstname and lastname in mails template ([0725978](https://gitlab.mim-libre.fr/alphabet/sondage/commit/07259781a426e0e698ae9429ba1d6b9c4acc0a74))
* **poll:** pre-complete info from laboite ([dea291b](https://gitlab.mim-libre.fr/alphabet/sondage/commit/dea291b733bb8cbf6d3bad3e8bc31985b82eef86))


### Bug Fixes

* **creation:** don't add null group at creation, simplify code ([7bafbe3](https://gitlab.mim-libre.fr/alphabet/sondage/commit/7bafbe35bf14cd137262acfc46083100b54c2b19))
* **smtp:** change call to smtp settings ([4baa26b](https://gitlab.mim-libre.fr/alphabet/sondage/commit/4baa26bf8c333926ef0c9b78d22da34ed0165e6b))
* **ui:** fix css import at first load and correct favicon ([b2ff775](https://gitlab.mim-libre.fr/alphabet/sondage/commit/b2ff775930d5fd9e24c944f3266f4928e91e21a8))


### Build System

* **meteor:** update meteor 2.7.3 and node version 14.19.3 ([9003ae7](https://gitlab.mim-libre.fr/alphabet/sondage/commit/9003ae7e15b5dbb42eb38e003f17b05f971bf476))
* **npm:** change npm command run start-dev to start and port to 3010 ([7634593](https://gitlab.mim-libre.fr/alphabet/sondage/commit/763459370734bba009b6eb7145c646d2f111e8d6))


### Documentation

* **licence:** add licence in source code and package ([5959f25](https://gitlab.mim-libre.fr/alphabet/sondage/commit/5959f25f2a8a3c6452912de483924ab69bb5ba97))
* **readme:** update and translate doc files ([0c466e3](https://gitlab.mim-libre.fr/alphabet/sondage/commit/0c466e3181d2318c22d1bffdd902fe4e0f09a36d))
* **settings:** add readme and update settings sample ([07be2f5](https://gitlab.mim-libre.fr/alphabet/sondage/commit/07be2f527839ca24e8e1da179b355514cbc156dc))


### Continuous Integration

* **build-docker:** run for `testing` prerelease ([1f694ef](https://gitlab.mim-libre.fr/alphabet/sondage/commit/1f694ef795aeff0c336ad67d3225c1f25e1b2deb))
* **commitlint:** use new standard job `.git:commitlint` ([03f7c53](https://gitlab.mim-libre.fr/alphabet/sondage/commit/03f7c53c6069f4a9bafd87c1399075c224c9df81))
* **merge-to-dev:** use new standard jobs `.git:merge-to` ([c90b1a4](https://gitlab.mim-libre.fr/alphabet/sondage/commit/c90b1a4e6f808cf8f7dc78d8988651e55c88e9b5))
* **meteor:** test before generating a new release ([28b3b5a](https://gitlab.mim-libre.fr/alphabet/sondage/commit/28b3b5a92021483755fbf2283680fc8ce4418911))
* **semantic-release:** create `testing` prerelease ([996de70](https://gitlab.mim-libre.fr/alphabet/sondage/commit/996de703b1c91c351638aca8e86ee6b1ab6e68a8))
* **tag docker:** tag `testing` prerelease image ([1f43f0a](https://gitlab.mim-libre.fr/alphabet/sondage/commit/1f43f0af09f15c4448ddc70e38d6352e3a1ceab8))

# [1.3.0](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/1.2.0...release/1.3.0) (2022-07-06)


### Bug Fixes

* **config:** display warning if api key is not set ([8c338d8](https://gitlab.mim-libre.fr/alphabet/sondage/commit/8c338d814abe34e96ef2cb450788f2fee9081d95))
* **description:** limit description max length ([cd55beb](https://gitlab.mim-libre.fr/alphabet/sondage/commit/cd55beb675658cc533d2c67c5e4a0d008000065a))
* **email:** change validation for email field ([83a3aac](https://gitlab.mim-libre.fr/alphabet/sondage/commit/83a3aac89216f649c83d5e76715ef1f769d406dd))
* **form:** fix slow email input in public pool ([fd170cf](https://gitlab.mim-libre.fr/alphabet/sondage/commit/fd170cf96b3bd3a36650f7fd2aa56ca0e65a02f3))
* **lint:** ignore new js font file ([5c1d845](https://gitlab.mim-libre.fr/alphabet/sondage/commit/5c1d8452df67a1bfc7a851f9b0310f8fbae99a10))
* **login:** deny user creation, redirect to laboite ([c7e64fc](https://gitlab.mim-libre.fr/alphabet/sondage/commit/c7e64fc197ccf800cc7760afc47dfab5a91aa9f0))
* **meeting:** display list mode by default ([c8c01a6](https://gitlab.mim-libre.fr/alphabet/sondage/commit/c8c01a66e3ffb353a3121cc562a5efda1a4bf95f))
* **meeting:** hide other users name if not owner ([8f38fd7](https://gitlab.mim-libre.fr/alphabet/sondage/commit/8f38fd721d5c1b5552180c0b4ee6e63fb80af9f2))
* **poll:** add name on poll answer instead of email ([824b577](https://gitlab.mim-libre.fr/alphabet/sondage/commit/824b5778c4e54aafba2bf43577de1bd42b4d7797))
* **poll:** rework regex to allow long email for pool answer ([4430ba6](https://gitlab.mim-libre.fr/alphabet/sondage/commit/4430ba6a9a32e0e770d53b0efbd8a83b9614650f))
* **poll:** show others users name in poll display ([9b3904d](https://gitlab.mim-libre.fr/alphabet/sondage/commit/9b3904dbef9bce247346ec9231ecdb8c1d9b9b91))
* set default timezone to europe/paris ([bd59d85](https://gitlab.mim-libre.fr/alphabet/sondage/commit/bd59d85d2e741c3af1980161a7876dd53db227f5))
* **title:** fix title max length ([39dbae3](https://gitlab.mim-libre.fr/alphabet/sondage/commit/39dbae39f4984d8e250606b81434c142d6bd574e))
* **ui:** bad title in create/edit first step ([7da5a78](https://gitlab.mim-libre.fr/alphabet/sondage/commit/7da5a78bbbfe37525b36897e8b1c880573d73bbd))
* **ui:** change container size for logout menu ([3dd7a8d](https://gitlab.mim-libre.fr/alphabet/sondage/commit/3dd7a8dc6b084a0074730d126f2cfe8779924656))
* **ui:** change logo alt attribut ([6a53108](https://gitlab.mim-libre.fr/alphabet/sondage/commit/6a531088db8a8df279434249ec4a2e1629740fd0))
* **ui:** change logo's path ([b36e303](https://gitlab.mim-libre.fr/alphabet/sondage/commit/b36e303180cbef2284c30c0bf899e9fbd1d15fd2))
* **ui:** fix avatar size in navbar for firefox ([b2bad2f](https://gitlab.mim-libre.fr/alphabet/sondage/commit/b2bad2f19e3712c4a769131d3f639f2fa0b6a15e))
* **ui:** fix language picker ([6694ed7](https://gitlab.mim-libre.fr/alphabet/sondage/commit/6694ed7fee1c815d8c5c25b07824b02ff8fef0ab))
* **ui:** hide scroll bar when is not requiered ([ef95666](https://gitlab.mim-libre.fr/alphabet/sondage/commit/ef956667234a84330469f2395a9e4b6ba39eb17f))
* **ui:** update french translation ([6ce9dcc](https://gitlab.mim-libre.fr/alphabet/sondage/commit/6ce9dcc023ddf0cbd38c498008fc8cc7f921decb))
* **ui:** update libraries ([77151bc](https://gitlab.mim-libre.fr/alphabet/sondage/commit/77151bc38f0056420d64ef85ed6ed04110d8334e))
* **ui:** use fontansome icon with local import ([d3b4460](https://gitlab.mim-libre.fr/alphabet/sondage/commit/d3b446095df24de4e37fb9b091094c65a697999c))


### Build System

* **meteor:** update meteor 2.7.2 ([a5ce429](https://gitlab.mim-libre.fr/alphabet/sondage/commit/a5ce42952c445ab97cded25bb3909d8ab9d7178a))


### Code Refactoring

* **menu:** new user menu style ([fe0b62e](https://gitlab.mim-libre.fr/alphabet/sondage/commit/fe0b62e6113f981ce56bb67bd85eb9f6c599fd84))
* **ui:** refactor maintenance page ([2d07b2a](https://gitlab.mim-libre.fr/alphabet/sondage/commit/2d07b2ac2216efdf82ff66dcb34a53aba04fcbb3))


### Features

* **maintenance:** lock application if laboite is in maintenance mode ([f2225b9](https://gitlab.mim-libre.fr/alphabet/sondage/commit/f2225b9cd6268840a99bd027aa4282f90242ce86))
* **meeting:** hide other users email, colorize slots ([5353fd3](https://gitlab.mim-libre.fr/alphabet/sondage/commit/5353fd3272b6fd0ddb1fa97a6bbc63e6955528b9))
* **menu:** fix size of logout button ([9ef8706](https://gitlab.mim-libre.fr/alphabet/sondage/commit/9ef87069c36358d382985ef595eb998b06eabf64))
* **poll:** add name field in poll and meeting answer ([80f07f0](https://gitlab.mim-libre.fr/alphabet/sondage/commit/80f07f01cd53fcbd28fb81116871bd16667ac198))
* **ui:** add text in metting and pools answer ([e959220](https://gitlab.mim-libre.fr/alphabet/sondage/commit/e959220b11777a39f5fcaf9012cf8bca568248d9))
* **ui:** fix user menu for firefox ([1469a24](https://gitlab.mim-libre.fr/alphabet/sondage/commit/1469a24281584b17e4627634caab5d178ca58d11))

# [1.2.0](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/1.1.0...release/1.2.0) (2022-05-05)


### Bug Fixes

* **audit:** update Dockerfile and CI ([b302cfc](https://gitlab.mim-libre.fr/alphabet/sondage/commit/b302cfc3f848dc4fff9078d7b5c5f2d44d1277c4))
* **audit:** update meteor and libraries ([8a2b70e](https://gitlab.mim-libre.fr/alphabet/sondage/commit/8a2b70e5c0dea2b8f330d7ddae18d802938cf830))
* **audit:** update to Meteor 2.7.1 ([81c15e5](https://gitlab.mim-libre.fr/alphabet/sondage/commit/81c15e53963cbe340cd2016e519004a9fb55d56d))
* **polls:** total number of polls/meetings is not detected correctly ([ce5afc0](https://gitlab.mim-libre.fr/alphabet/sondage/commit/ce5afc0202adb4041dba20e1224a267ade753bfa))


### Code Refactoring

* **ui:** change spinner logo in loader file ([559fb4b](https://gitlab.mim-libre.fr/alphabet/sondage/commit/559fb4b5c4b10cbba805337e562f2697e7f2d043))


### Features

* **ui:** change logo and fit nav bar with new logo ([cb0104a](https://gitlab.mim-libre.fr/alphabet/sondage/commit/cb0104a7b739bc9fae9967b100435832cc539094))

# [1.1.0](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/1.0.3...release/1.1.0) (2022-03-11)


### Features

* **autologin:** work in progress for autologin feature ([48062fd](https://gitlab.mim-libre.fr/alphabet/sondage/commit/48062fd084a279d77c81a3f73ab96c15d9771f4b))

## [1.0.3](https://gitlab.mim-libre.fr/alphabet/sondage/compare/release/1.0.2...release/1.0.3) (2022-01-19)


### Bug Fixes

* **backend:** fix poll date validation ([09b153c](https://gitlab.mim-libre.fr/alphabet/sondage/commit/09b153c4f5df6f6d22c792b062475699a6a2981e))
* **backend:** fix poll date validation ([686c52a](https://gitlab.mim-libre.fr/alphabet/sondage/commit/686c52a4ab48e7a8d1907805758911010352fb51))
* **backend:** reduce duplicate participants on poll date validation ([d506a8b](https://gitlab.mim-libre.fr/alphabet/sondage/commit/d506a8b4e9132033b15d3e6855b3cb8e1bc1cb20))
* **gitlab-ci:** some globally defined keywords are deprecated ([4131ec1](https://gitlab.mim-libre.fr/alphabet/sondage/commit/4131ec160fb06706e9640e788e3772730599f056))
* **libs:** switch back to official package for mexar:mdt ([7913abe](https://gitlab.mim-libre.fr/alphabet/sondage/commit/7913abe01486633c28591f60c589d8fac4117119))
* **lib:** update dependencies ([4abb19c](https://gitlab.mim-libre.fr/alphabet/sondage/commit/4abb19c57ef2a30185d071e33721b8d53ee06009))
* **poll event:** flatten array of participations when creating event ([e343415](https://gitlab.mim-libre.fr/alphabet/sondage/commit/e343415417f8ac212fe5f2de30a6f5d3ebd18b27))
* **semantic-release):** the stable branch is `master` ([1534231](https://gitlab.mim-libre.fr/alphabet/sondage/commit/1534231e0f9cd27f662f14053853eb159331f928))
* **ui:** fix all day polls validation and count dates ([b9c1d82](https://gitlab.mim-libre.fr/alphabet/sondage/commit/b9c1d824d7e297b37aa967167a65d250adfb9d4e))
* **ui:** fix dates count for rdv ([79d9912](https://gitlab.mim-libre.fr/alphabet/sondage/commit/79d9912f630bdce0214e46b1287698709fd7cef0))
* **ui:** various enhancements for poll/meeting ui ([a0d9fb3](https://gitlab.mim-libre.fr/alphabet/sondage/commit/a0d9fb36478ea3f1db4c27c1e1d25fa85944646f))
* **version:** update testing version ([3d29129](https://gitlab.mim-libre.fr/alphabet/sondage/commit/3d29129161037bcbb6fd2b89decdce15a544a141))
* **version:** update version number for testing ([cc64bc8](https://gitlab.mim-libre.fr/alphabet/sondage/commit/cc64bc8d1d9755185582d9a3ddeb367c9e05a495))
* **version:** update version number for testing ([1ac3146](https://gitlab.mim-libre.fr/alphabet/sondage/commit/1ac3146ae10ffcd78d7b8265181cdf891a9611c0))


### Continuous Integration

* **build:** create the docker image and push it to `${CI_REGISTRY}` ([092bcd5](https://gitlab.mim-libre.fr/alphabet/sondage/commit/092bcd5e1f86cdaebecda1a02acb7678835fb39d))
* **commitlint:** enforce commit message format ([1ecaef7](https://gitlab.mim-libre.fr/alphabet/sondage/commit/1ecaef70d90834b19873fabf31a832c275c9ef7d))
* **release:** avoid regression in `dev` branch ([1b90833](https://gitlab.mim-libre.fr/alphabet/sondage/commit/1b9083346c1c1d8e87240f4e4a3b91adb1b2e808))
* **release:** create release automatically with `semantic-release` ([8d5af63](https://gitlab.mim-libre.fr/alphabet/sondage/commit/8d5af63790de78af7501775778caafe997ffd422))
* **release:** tag docker images based on release cycle ([d613b90](https://gitlab.mim-libre.fr/alphabet/sondage/commit/d613b90ce945ed45f8486b7e5bfab0b16e9e4119))
* **rules:** restrict execution to non stable branches by default ([779bd43](https://gitlab.mim-libre.fr/alphabet/sondage/commit/779bd439760952c759c16c389b95e9d697484a8a))
* **runners:** use OpenNebula runners with shared cache ([baccb68](https://gitlab.mim-libre.fr/alphabet/sondage/commit/baccb682dbb383275c09dfad896f5e18ced9cfe9))


### Styles

* **gitlab-ci:** better self explanatory job names ([f86b4ee](https://gitlab.mim-libre.fr/alphabet/sondage/commit/f86b4ee5a1cdec5103706e1087edd8a829165a53))
