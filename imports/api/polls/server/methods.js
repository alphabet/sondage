import { Meteor } from 'meteor/meteor';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import SimpleSchema from 'simpl-schema';
import moment from 'moment';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SyncedCron } from 'meteor/littledata:synced-cron';

import Polls from '../polls';
import Groups from '../../groups/groups';
import PollsAnswers from '../../polls_answers/polls_answers';
import { sendEmail, createEventAgenda } from '../../events/server/methods';
import { validatePoll } from '../methods';
import getExpirationDate from '../../../utils/functions/polls';
import RegEx from '../../../utils/regExp';
import logger from '../../../utils/logger';

export const getSinglePoll = new ValidatedMethod({
  name: 'polls.getSinglePoll',
  validate: new SimpleSchema({
    pollId: {
      type: String,
      regEx: RegEx.Id,
    },
  }).validator({ clean: true }),

  run({ pollId }) {
    logger.info({ message: 'Poll getted', method: 'getSinglePoll', params: { userId: this.userId, pollId } });
    return Polls.findOne({ _id: pollId, userId: this.userId });
  },
});

export const getSinglePollToAnswer = new ValidatedMethod({
  name: 'polls.getSinglePollToAnswer',
  validate: new SimpleSchema({
    pollId: {
      type: String,
      regEx: RegEx.Id,
    },
  }).validator({ clean: true }),

  run({ pollId }) {
    logger.info({ message: 'Poll getted', method: 'getSinglePollToAnswer', params: { userId: this.userId, pollId } });
    const poll = Polls.findOne({ _id: pollId });
    if (!poll) {
      logger.error({
        message: 'api.errors.notFound',
        method: 'getSinglePollToAnswer',
        params: { userId: this.userId, pollId },
      });
      throw new Meteor.Error('api.polls.methods.get.notFound', `api.errors.notFound`);
    }
    const isInAGroup = Groups.findOne(
      {
        _id: { $in: poll.groups },
        $or: [
          { admins: { $in: [this.userId] } },
          { animators: { $in: [this.userId] } },
          { members: { $in: [this.userId] } },
        ],
      },
      { fields: { _id: 1 } },
    );
    const author = Meteor.users.findOne(poll.userId) || { firstName: '', lastName: '' };
    const data = {
      poll,
      author: { firstName: author.firstName, lastName: author.lastName },
      selectedGroups: Groups.find({ _id: { $in: poll.groups } }).fetch(),
      answer: this.userId ? PollsAnswers.findOne({ pollId, userId: this.userId }) : null,
    };
    if ((!poll.public && !this.userId) || (!isInAGroup && !poll.public && this.userId !== poll.userId)) {
      logger.error({
        message: 'api.errors.notFound',
        method: 'getSinglePollToAnswer',
        params: { userId: this.userId, pollId },
      });
      throw new Meteor.Error('api.polls.methods.get.notPublic', `api.errors.notApublicPoll_${poll.type}`);
    }
    return data;
  },
});

export const updatePoll = new ValidatedMethod({
  name: 'polls.update',
  validate: new SimpleSchema({
    data: Polls.schema.omit('createdAt', 'updatedAt'),
    pollId: {
      type: String,
      regEx: RegEx.Id,
    },
  }).validator({ clean: true }),

  run({ data, pollId }) {
    // check if logged in
    if (!this.userId) {
      logger.error({
        message: 'api.errors.notLoggedIn',
        method: 'updatePoll',
        params: { userId: this.userId, pollId, data },
      });
      throw new Meteor.Error('api.polls.methods.update.notLoggedIn', 'api.errors.notLoggedIn');
    }

    const poll = Polls.findOne(pollId) || {};
    if (this.userId !== poll.userId) {
      logger.error({
        message: 'api.errors.notAllowed',
        method: 'updatePoll',
        params: { userId: this.userId, pollId, data },
      });
      throw new Meteor.Error('api.polls.methods.update.notAllowed', 'api.errors.notAllowed');
    } else if (poll.active || poll.completed) {
      logger.error({
        message: 'api.errors.notAllowed',
        method: 'updatePoll',
        params: { userId: this.userId, pollId, data },
      });
      throw new Meteor.Error('api.polls.methods.update.active', 'api.errors.notAllowed');
    }
    validatePoll(data);
    logger.info({
      message: 'Polls updated',
      method: 'updatePoll',
      params: { userId: this.userId, pollId, data },
    });
    return Polls.update({ _id: pollId }, { $set: { ...data } });
  },
});

export const validatePollAnswer = new ValidatedMethod({
  name: 'polls.validate',
  validate: new SimpleSchema({
    pollId: {
      type: String,
      regEx: RegEx.Id,
    },
    date: Date,
  }).validator({ clean: true }),

  run({ pollId, date }) {
    const poll = Polls.findOne({ _id: pollId });

    if (poll.userId !== this.userId) {
      logger.error({
        message: 'api.errors.notAllowed',
        method: 'validatePollAnswer',
        params: { userId: this.userId, ownerId: poll.userId, pollId },
      });
      throw new Meteor.Error('api.polls_answers.methods.validate.notAllowed', 'api.errors.notAllowed');
    } else if (poll.completed) {
      logger.error({
        message: 'Poll not completed',
        method: 'validatePollAnswer',
        params: { userId: this.userId, ownerId: poll.userId, pollId },
      });
      throw new Meteor.Error('api.polls_answers.methods.validate.notAllowed', 'api.errors.notAllowed');
    }

    createEventAgenda(poll, date, this.userId);
    if (poll.groups.length) {
      if (!Meteor.isTest) {
        // eslint-disable-next-line global-require
        const sendnotif = require('../../notifications/server/notifSender').default;
        logger.info({
          message: 'Notification send',
          method: 'createEventAgenda',
          params: { userId: this.userId, date, pollId },
        });
        sendnotif({
          groups: poll.groups,
          title: 'Nouvel évenement',
          content: `la date ${moment(date).format('LL')} a été retenue pour "${poll.title}"`,
          pollId: poll._id,
          internalLink: '/events',
          type: poll.type,
        });
      }
    }
    const result = Polls.update({ _id: pollId }, { $set: { completed: true, choosenDate: date, active: false } });
    // find answers of laboite users
    let answers = PollsAnswers.find({ userId: { $ne: null }, pollId }).fetch();
    if (poll.public) {
      // for public polls, add non laboite users answers
      const connectedAnswers = PollsAnswers.find({ userId: null, pollId }).fetch();
      answers = [...answers, ...connectedAnswers];
    }
    // send email to all answerers
    let emailErrors = false;
    answers.forEach((answer) => {
      try {
        sendEmail(poll, {
          ...answer,
          meetingSlot: date,
        });
      } catch (error) {
        emailErrors = true;
      }
    });
    if (emailErrors) {
      logger.error({
        message: 'api.errors.cannotSendEmail',
        method: 'createEventAgenda',
        params: { userId: this.userId, date, pollId },
      });
      throw new Meteor.Error('api.events.methods.sendEmail', 'api.errors.cannotSendEmail');
    }
    logger.info({
      message: 'Event created in Agenda',
      method: 'createEventAgenda',
      params: { userId: this.userId, date, pollId },
    });
    return result;
  },
});

SyncedCron.add({
  name: 'Delete expired polls',
  schedule: function removeSchedule(parser) {
    return parser.text(`at 3:00 am`);
  },
  job: function removeOldPolls() {
    // update old polls without expiresAt field
    Polls.find({ expiresAt: null })
      .fetch()
      .forEach((oldPoll) => {
        // calculate default expirationDate (7 days after last poll/meeting date)
        const expDate = getExpirationDate(oldPoll);
        Polls.update({ _id: oldPoll._id }, { $set: { expiresAt: expDate } });
      });
    // remove expired polls
    const expiredPolls = Polls.find({
      expiresAt: {
        $lt: new Date(),
      },
    }).fetch();
    expiredPolls.forEach((poll) => {
      PollsAnswers.remove({ pollId: poll._id });
      Polls.remove({ _id: poll._id });
      logger.error({
        message: 'Poll deleted',
        method: 'sondageCron',
        params: { pollId: poll._id },
      });
    });
    return expiredPolls.length;
  },
});

const methodsKeys = [
  'polls.create',
  'polls.remove',
  'polls.getSinglePoll',
  'polls.update',
  'polls.getSinglePollToAnswer',
  'polls.validate',
];
DDPRateLimiter.addRule(
  {
    name(name) {
      return methodsKeys.find((m) => m === name);
    },
    connectionId() {
      return true;
    },
  },
  1,
  300,
);
