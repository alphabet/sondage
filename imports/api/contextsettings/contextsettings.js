import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const ContextSettings = new Mongo.Collection('contextsettings');

// Deny all client-side updates since we will be using methods to manage this collection
ContextSettings.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

ContextSettings.schema = new SimpleSchema(
  {
    key: {
      type: String,
    },
    type: {
      type: String,
    },
    value: {
      type: Array,
    },
    'value.$': {
      type: { any: {} },
    },
  },
  { clean: { removeEmptyStrings: false } },
);

ContextSettings.publicFields = {
  key: 1,
  value: 1,
};

ContextSettings.attachSchema(ContextSettings.schema);

export default ContextSettings;
