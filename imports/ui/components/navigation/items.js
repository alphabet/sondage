import { ROUTES } from '../../../utils/enums';

export const items = [
  {
    text: 'links.polls',
    path: ROUTES.POLLS,
  },
  {
    text: 'links.home',
    path: ROUTES.ADMIN,
  },
  {
    text: 'links.new_poll',
    path: ROUTES.REDIRECT,
  },
];

export default items;
