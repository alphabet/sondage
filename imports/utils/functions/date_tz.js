function formatDateTZ(date, tz) {
  // Returns a formatted string from a date and a timezone
  // formatDateTZ(new Date(),"Europe/Paris")
  // => "2024-03-19 17:47:00"

  return date.toLocaleString('sv-SE', { timeZone: tz });
}

export default formatDateTZ;
