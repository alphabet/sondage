import { POLLS_TYPES } from '../enums';

function getExpirationDate(poll) {
  // determines default poll expiration date based on poll/meetings dates
  let expDate = new Date();
  if (!poll.expiresAt) {
    if (poll.type === POLLS_TYPES.POLL) {
      const pollDates = poll.dates.sort((a, b) => a - b).reverse();
      if (pollDates.length > 0) expDate = new Date(pollDates[0]);
    } else {
      const meetingDates = poll.meetingSlots
        .map((slot) => slot.end)
        .sort((a, b) => a - b)
        .reverse();
      if (meetingDates.length > 0) expDate = new Date(meetingDates[0]);
    }
    // expiration date: last day defined in poll + 1 month
    expDate.setMonth(expDate.getMonth() + 1);
  } else {
    expDate = poll.expiresAt;
  }
  return expDate;
}

export default getExpirationDate;
